
# CONTENTS OF THIS FILE
  
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * FAQ
 * Maintainers
 
## INTRODUCTION

This module allows to display term definitions into your node contents.
Definitions are provided by TerMef, a service of the French Economic
and Financial Ministries (Mef).

## REQUIREMENTS

This module does not require any other module.

## INSTALLATION:

1. Download and enable as normal module;
2. Go to the Formats list and edit format which you want to use Termef with.

## CONFIGURATION

* You can enable/disable the Termef tag filter per format.
* You can enable the Termef button in CKEditor.

## UNINSTALLING:

Uninstall as a normal module.

## Maintainers

See https://www.drupal.org/project/termef.
