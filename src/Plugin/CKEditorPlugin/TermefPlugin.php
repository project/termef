<?php

namespace Drupal\termef\Plugin\CKEditorPlugin;

use Drupal\editor\Entity\Editor;
use Drupal\ckeditor\CKEditorPluginBase;

/**
 * Defines the "termef" plugin.
 *
 * @CKEditorPlugin(
 *   id = "termef",
 *   label = @Translation("Termef link"),
 *   module = "termef"
 * )
 */
class TermefPlugin extends CKEditorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getFile() {
    return drupal_get_path('module', 'termef') . '/js/plugins/plugin.js';
  }

  /**
   * {@inheritdoc}
   */
  public function getDependencies(Editor $editor) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getLibraries(Editor $editor) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function isInternal() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor) {
    return [
      'termef_dialogTitleAdd' => $this->t('Insert Termef Definition'),
      'termef_dialogTitleEdit' => $this->t('Edit Termef Definition'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getButtons() {
    return [
      'Termef' => [
        'label' => $this->t('Termef'),
        'image' => drupal_get_path('module', 'termef') . '/js/plugins/icons/termef.png',
      ],
    ];
  }

}
