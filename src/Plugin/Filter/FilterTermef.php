<?php

namespace Drupal\termef\Plugin\Filter;

use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;

/**
 * Provides a filter to insert termef definitions.
 *
 * @Filter(
 *   id = "filter_termef",
 *   title = @Translation("Termef tag"),
 *   description = @Translation("Uses a <code>[termef:value]</code> tag to display Termef definitions."),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_MARKUP_LANGUAGE
 * )
 */
class FilterTermef extends FilterBase {

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {
    $pattern = '/\[termef:([^\]]+)\]/';
    $result = preg_match_all($pattern, $text, $matches);
    if ($result === FALSE) {
      $result = new FilterProcessResult($text);
      return $result;
    }
    foreach ($matches[0] as $key => $search) {
      $match = $matches[1][$key];
      $pieces = explode('|', $match);
      if (count($pieces) == 2) {
        $query = $pieces[0];
        $label = $pieces[1];
      }
      else {
        $query = $label = $match;
      }
      $infobox = [
        '#theme' => 'termef_infobox',
        '#query' => $query,
        '#label' => $label,
      ];
      $replace = \Drupal::service('renderer')->render($infobox);
      $text = str_replace($search, $replace, $text);
    }
    $result = new FilterProcessResult($text);
    $result->addAttachments([
      'library' => [
        'termef/infobox',
      ],
    ]);
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function tips($long = FALSE) {
    if ($long) {
      return $this->t('
        <p>You can insert TerMef definitions. Examples:</p>
        <ul>
          <li><code>[termef:pib]</code></li>
        </ul>');
    }
    else {
      return $this->t('You can insert TerMef definitions ([termef:pib]).');
    }
  }

}
