<?php

namespace Drupal\termef\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\termef\TermefInfobox;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\Core\Render\RendererInterface;

/**
 * Controller for the Termef module.
 */
class TermefController extends ControllerBase {

  /**
   * Infobox service.
   *
   * @var \Drupal\termef\TermefInfobox
   */
  protected $infobox;

  /**
   * Request service.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Drupal\Core\Render\RendererInterface definition.
   *
   * @var Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * TermefController constructor.
   *
   * @param \Drupal\termef\TermefInfobox $infobox
   *   The infobox service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack service.
   * @param Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   */
  public function __construct(TermefInfobox $infobox, RequestStack $request_stack, RendererInterface $renderer) {
    $this->infobox = $infobox;
    $this->requestStack = $request_stack;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('termef.infobox'),
      $container->get('request_stack'),
      $container->get('renderer')
    );
  }

  /**
   * API Callback.
   *
   * @return string
   *   HTML markup for a tooltip
   */
  public function api($query) {
    $values = $this->infobox->query($query);
    if (empty($values)) {
      return new JsonResponse($values, 404);
    }
    $tooltip = [
      '#theme' => 'termef_tooltip',
      '#values' => $values,
    ];
    $tooltip = $this->renderer->render($tooltip);
    return new JsonResponse($tooltip);
  }

  /**
   * Autocomplete Callback.
   *
   * @return string
   *   JSON Response for autocomplete
   */
  public function autocomplete($query = FALSE) {
    $parameters = $this->requestStack->getCurrentRequest()->query->all();
    if (empty($parameters['q'])) {
      return new JsonResponse();
    }
    if (strlen($parameters['q']) < 2) {
      return new JsonResponse();
    }
    $query = $parameters['q'];
    $mode = $parameters['mode'];
    $values = $this->infobox->autocomplete($query, $mode);
    return new JsonResponse($values);
  }

}
