<?php

namespace Drupal\termef\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\editor\Ajax\EditorDialogSave;
use Drupal\Core\Ajax\CloseModalDialogCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Termef CKEditor Dialog form.
 */
class TermefCKEditorDialog extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'termef_ckeditor_dialog';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $input = $form_state->getUserInput();
    $values = $form_state->getValues();
    $mode = isset($values['mode']) ? $values['mode'] : 'begin';

    $form['#tree'] = TRUE;
    $form['#attached']['library'][] = 'editor/drupal.editor.dialog';
    $form['#attached']['library'][] = 'termef/autocomplete';
    $form['#prefix'] = '<div id="termef-ckeditor-dialog-form">';
    $form['#suffix'] = '</div>';

    $form['mode'] = [
      '#type' => 'radios',
      '#title' => $this->t('Search mode'),
      '#options' => [
        'begin' => $this->t('starts with'),
        'exact' => $this->t('exactly'),
      ],
      '#default_value' => $mode,
      '#ajax' => [
        'callback' => [$this, 'changeSearchMode'],
        'event' => 'change',
        'wrapper' => 'term-wrapper',
      ],
      '#prefix' => '<div id="mode-wrapper">',
      '#suffix' => '</div>',
    ];

    $form['term'] = [
      '#type' => 'textfield',
      '#autocomplete_route_name' => 'termef.autocomplete',
      '#autocomplete_route_parameters' => ['mode' => $mode],
      '#title' => $this->t('Search for referent term'),
      '#required' => TRUE,
      '#prefix' => '<div id="term-wrapper">',
      '#suffix' => '</div>',
      '#default_value' => isset($input['editor_object']['label']) ? $input['editor_object']['label'] : NULL,
    ];

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Term to display (if different from referent term)'),
      '#prefix' => '<div id="display-wrapper">',
      '#suffix' => '</div>',
      '#default_value' => isset($input['editor_object']['label']) ? $input['editor_object']['label'] : NULL,
    ];

    $warning = $this->t(
      '<p><em>The term you are looking for does not exist? Send a message to <a href=":mailto">:email</a> to request that it be added.</em></p>',
      [':mailto' => 'mailto:terminologie@finances.gouv.fr', ':email' => 'terminologie@finances.gouv.fr']
    );
    $form['warning'] = [
      '#type' => 'item',
      '#markup' => $warning,
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['save_modal'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      // No regular submit-handler. This form only works via JavaScript.
      '#submit' => [],
      '#ajax' => [
        'callback' => '::submitForm',
        'event' => 'click',
      ],
    ];

    return $form;
  }

  /**
   * Ajax callback : search mode update.
   */
  public function changeSearchMode(array &$form, FormStateInterface $form_state) {
    return $form['term'];
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    if ($form_state->getErrors()) {
      unset($form['#prefix'], $form['#suffix']);
      $form['status_messages'] = [
        '#type' => 'status_messages',
        '#weight' => -10,
      ];
      $response->addCommand(new HtmlCommand('#termef-ckeditor-dialog-form', $form));
    }
    else {
      $values = [];
      $values['term'] = $form_state->getValue('term');
      $values['label'] = $form_state->getValue('label');
      $response->addCommand(new EditorDialogSave($values));
      $response->addCommand(new CloseModalDialogCommand());
    }
    return $response;
  }

}
