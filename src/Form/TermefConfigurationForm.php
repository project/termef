<?php

namespace Drupal\termef\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Component\Utility\UrlHelper;

/**
 * Termef Configuration form.
 */
class TermefConfigurationForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['termef.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'termef_configuration_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('termef.settings');
    $form['callback'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Url'),
      '#description' => $this->t('Please provide the API url to use.'),
      '#default_value' => $config->get('callback'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $callback = $form_state->getValue('callback');
    if (!UrlHelper::isValid($callback, TRUE)) {
      $form_state->setErrorByName('callback', $this->t('This url is not valid.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('termef.settings')
      ->set('callback', $form_state->getValue('callback'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
