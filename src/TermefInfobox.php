<?php

namespace Drupal\termef;

use Drupal\Core\Config\ConfigFactoryInterface;
use GuzzleHttp\ClientInterface;
use Drupal\Component\Serialization\Json;

/**
 * Infobox Service for Termef.
 */
class TermefInfobox {

  /**
   * Config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Http Client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * TermefInfobox constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config factory.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   Http Client.
   */
  public function __construct(ConfigFactoryInterface $config_factory, ClientInterface $http_client) {
    $this->configFactory = $config_factory;
    $this->httpClient = $http_client;
  }

  /**
   * Returns the infobox answer.
   *
   * @return string
   *   Formated array from API answer.
   */
  public function query($query) {
    $config = $this->configFactory->get('termef.settings');
    $callback = $config->get('callback');
    $url = sprintf('%s?q=%s&s=1&mode=exact&field=label', $callback, $query);
    try {
      $request = $this->httpClient->get($url);
      $status = $request->getStatusCode();
      $data = $request->getBody()->getContents();
    }
    catch (RequestException $e) {
      return FALSE;
    }
    if ($status != 200) {
      return FALSE;
    }
    $decoded = Json::decode($data);
    if (empty($decoded['items'][0]['attributes'])) {
      return FALSE;
    }
    $item = [];
    // Liste des attributs.
    $attributes = $decoded['items'][0]['attributes'];
    foreach ($attributes as $attribute) {
      if ($attribute['type'] == 'text') {
        $item[$attribute['attribute']] = implode('<br>', $attribute['values']);
      }
      elseif ($attribute['type'] == 'ancestors') {
        foreach ($attribute['values'] as $ancestors) {
          $entries = [];
          foreach ($ancestors as $ancestor) {
            $title = isset($ancestor['display']) ? $ancestor['display'] : $ancestor['name'];
            $entries[] = ['title' => $title, 'url' => $ancestor['value']];
          }
          $item[$attribute['attribute']][] = $entries;
        }
      }
    }
    // Ajout de l'url.
    $url = $decoded['items'][0]['url'];
    if (!empty($url)) {
      $item['graphe'] = $url;
    }
    return $item;
  }

  /**
   * Returns the infobox autocomplete suggestions.
   *
   * @return array
   *   Array of suggestions for autocomplete.
   */
  public function autocomplete($query, $mode) {
    $suggestions = [];
    $config = $this->configFactory->get('termef.settings');
    $callback = $config->get('callback');
    $url = sprintf('%s?q=%s&mode=%s', $callback, $query, $mode);
    try {
      $request = $this->httpClient->get($url);
      $status = $request->getStatusCode();
      $data = $request->getBody()->getContents();
    }
    catch (RequestException $e) {
      return $suggestions;
    }
    if ($status != 200) {
      return $suggestions;
    }
    $decoded = Json::decode($data);
    if (empty($decoded['items'])) {
      return $suggestions;
    }
    foreach ($decoded['items'] as $item) {
      $value = $vocabulary = NULL;
      $attributes = $item['attributes'];
      foreach ($attributes as $attribute) {
        if ($attribute['type'] == 'text' && $attribute['attribute'] == 'Nom') {
          $value = implode(', ', $attribute['values']);
        }
        if ($attribute['type'] == 'ancestors' && $attribute['attribute'] == 'groupe') {
          $values = $attribute['values'];
          foreach ($values as $entry) {
            if ($entry['name'] == mb_convert_case($entry['name'], MB_CASE_UPPER)) {
              $vocabulary = $entry['display'];
              break(1);
            }
          }
        }
      }
      if (!empty($vocabulary)) {
        $label = sprintf('%s <span>(%s)</span>', $value, $vocabulary);
      }
      else {
        $label = $value;
      }
      $suggestion = ['label' => $label, 'value' => $value];
      $suggestions[] = $suggestion;
    }
    return $suggestions;
  }

}
