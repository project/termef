(function ($) {

  $(document).ready(function() {

    function triggerAutocomplete(x,y) {
      var value = $('#term-wrapper input').val();
      if (value != '') {
        $('#term-wrapper input').focus().val('').val(value).autocomplete('search',value);
      }
    }

    triggerAutocomplete();

    $("body").on("dialogopen",function(e,u){
      triggerAutocomplete();
    });

    $( document ).ajaxComplete(function(e) {
      triggerAutocomplete();
    });

  });


})(jQuery);