(function ($, Drupal, CKEDITOR) {
  // Register the plugin
  CKEDITOR.plugins.add('termef', {
    // The plugin initialization logic goes inside this method.
    init: function (editor) {
      // Add the commands for link and unlink.
      editor.addCommand('insertTermef', {
        modes: {wysiwyg: 1},
        exec: function (editor) {

          var selection = editor.getSelection();
          if (!selection) {
            var existingValues = {};
          } else {
            var label = selection.getSelectedText();
            var existingValues = {'label': label};
          }

          var saveCallback = function (data) {
            editor.fire('saveSnapshot');
            if (data.label != '') {
              termef = '[termef'+ ':' + data.term + '|' + data.label + ']';
            } else {
              termef = '[termef'+ ':' + data.term + ']';
            }
            editor.insertHtml( termef );
          }

          var dialogSettings = {
            title: Drupal.t('Insert a TerMef definition'),
            dialogClass: 'termef-dialog'
          };

          Drupal.ckeditor.openDialog(editor, Drupal.url('termef/dialog'), existingValues, saveCallback, dialogSettings);
        }
      });
      // Add buttons for link and unlink.
      if (editor.ui.addButton) {
        editor.ui.addButton('Termef', {
          label: Drupal.t('Insert a TerMef definition'),
          command: 'insertTermef',
          toolbar: 'insert',
          icon: this.path + 'icons/tmg.png'
        });
      }
    }
  });
})(jQuery, Drupal, CKEDITOR);