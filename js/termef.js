(function ($, Drupal, drupalSettings) {

   Drupal.behaviors.infobox = {

    attach: function (context, settings) {
      $('.termef .infobox', context).on('keypress', function (e) {
       var key = e.which;
        if(key == 13) {
          var container = $(this).parent('span.termef');
          var tooltip = container.find('.tooltip-container');
          if (tooltip.length === 1) {
            container.find('.infobox').removeClass('processed');
            container.find('.tooltip-container').remove();
          } else {
            $(this).click();
          }
          return false;  
        }
      });
      $('.termef .infobox', context).on('click', function () {
        var container = $(this).parent('span.termef');

        var tooltip = container.find('.tooltip-container');
        if (tooltip.length === 1) {
          return;
        }
        if ($(this).hasClass('processed')) {
          return;
        }

        $(this).addClass('processed');
        
        var search = $(this).attr('rel');
        var callback = '/termef/api/' + search;

        timer = setTimeout(function () {
          $.ajax({
            url : callback,
            type : 'GET',
            dataType : 'json',
            success : function(data, status){
              container.append(data);
              var tooltip = container.find('.tooltip-container');
              tooltip.draggable();
            },
            error : function(data, status){
              alert(Drupal.t('The service is temporarily unavailable'));
            }
          });
        }, 500);
      });

      $(document).on("click", '.more', function(event) {
        var less = $(this).parent('.more-container').parent('.content.less');
        var full = less.parent('.inner').find('.content.full');
        less.hide();
        full.show();
        return;
      });

      $(document).on("click", '.less', function(event) {
        var full = $(this).parent('.more-container').parent('.content.full');
        var less = full.parent('.inner').find('.content.less');
        full.hide();
        less.show();
      });

      $(document).on("click", '.close', function(event) {
        var termef = $(this).parentsUntil('.termef');
        termef.parent().find('.infobox').removeClass('processed');
        termef.remove('.tooltip-container');
        return;
      });

     }
 };
 })(jQuery, Drupal, drupalSettings);